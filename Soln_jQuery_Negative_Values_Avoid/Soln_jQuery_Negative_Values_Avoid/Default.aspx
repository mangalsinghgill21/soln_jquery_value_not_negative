﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_jQuery_Negative_Values_Avoid.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task value should not be negative in jquery</title>
     <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>

    <script type="text/javascript">

        function pageLoad()
        {
            $(document).ready(function ()
            {
                var number = parseInt($(this).find("#div1id").text(), 10);

                $("#<%=btnsubmit.ClientID%>").click(function ()
                {
                    if (number >0)
                    {
                        $("#div1id").addClass("positive");
                    }
                    else
                    {
                        $("#div1id").addClass("negative");
                    }
                   
                });
            });
        }

    </script>

    <link href="Css/css.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:center">
      <asp:TextBox ID="txtvalue" runat="server" placeHolder ="Enter Number" CssClass="txt" />
      <br />
        <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn2" />
       <br />
        <div id="div1id"> My color will change according to value (+ve or -ve). </div>
    
    </div>
    </form>
</body>
</html>
